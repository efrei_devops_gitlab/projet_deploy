data "aws_vpc" "web" {
  tags = {
    Name = "WebVpc"
  }
}

data "aws_security_group" "private" {
  tags = {
    Name = "Private"
  }
}

data "aws_security_group" "public" {
  tags = {
    Name = "Public"
  }
}
data "aws_ami" "web_ami" {
  most_recent = true
  owners      = ["self"]
  name_regex  = "^${var.AppName}"
}

data "aws_subnets" "private_subnets" {
  tags = {
    Name = "WebPrivate-*"
  }
}

data "aws_subnet" "private_subnets" {
  for_each = toset(data.aws_subnets.private_subnets.ids)
  id       = each.value
}

data "aws_subnets" "public_subnets" {
  tags = {
    Name = "WebPublic-*"
  }
}

data "aws_subnet" "public_subnets" {
  for_each = toset(data.aws_subnets.public_subnets.ids)
  id       = each.value
}


data "aws_internet_gateway" "web_igw" {
  tags = {
    Name = "Web_GW"
  }
}
