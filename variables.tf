variable "AppName" {
  type    = string
  default = "WebApp"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "apache_port" {
  default = 8080
}
